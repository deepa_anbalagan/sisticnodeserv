var express = require('express');
var router = express.Router();
const fs = require('fs')
const BASE_URL = "http://localhost:5000/";

router.post('/', function(req, res, next) {
    var constant = JSON.stringify(req.body.state);
    fs.writeFile('uploads/env.js', constant , err => {
        if (err) {
            console.error(err)
            return
        }else{
            console.log("File written successfully ==>", JSON.stringify(req.body.state));
            res.send(BASE_URL+"uploads/env.js");
        }

    })
});

module.exports = router;
